# Shibboleth for MRBS

[MRBS](http://mrbs.sourceforge.net/) is a reservation tool with various use cases.
It supports many authentication/authorization services. This projects adds 
Shibboleth to the list.

## How to use it

1. Clone/download the code
2. Copy the files to their location
  * **SessionShibboleth.inc** to the directory *lib/MRBS/Session*
  * **AuthShibboleth.inc** to the directory *lib/MRBS/Auth*
3. Set up MRBS to use shibboleth - modify **config.inc.php** this way:

```php
$auth["session"] = "shibboleth";
$auth["type"] = "shibboleth"; 
```

## Shibboleth attributes mapping

Since the names of attributes the SP receives by IDP differ the plugin needs 
to know their names. For this purpose these variables have to be defined:

```php
// URI used to create the shibboleth auth request.
$auth['shibboleth']['base_uri'] = "https://example.com";

// URI used to redirect to after Shibboleth does the authentication.
$auth['shibboleth']['site_uri'] = "https://example.com/mrbs/";

// Shibboleth handler URI
$auth['shibboleth']['handler_uri'] = "Shibboleth.sso";

// Attribute where the complete user name is stored. This is displayed after user logs in.
$auth['shibboleth']['name_attribute'] = "nickname";

// Attribute where the unique user name is stored.
$auth['shibboleth']['username_attribute'] = "uid";

// Attribute where email is stored.
$auth['shibboleth']['email_attribute'] = 'mail';
```

Shibboleth even might give enough information about user roles. In this case 
all you have to do to distinguish between user and administrator is to set
the names of attributes and possible values:

```php
// The set of Shibboleth attributes values that define a valid user
// only one item is needed, not all of them at once
$auth['shibboleth']['user_roles'] = array(
  'unscoped-affiliation' => array('student'),
  'RolesTypeB2' => array('13000:30','0:362', array('type' => 'regex', 'value' => '/13...:190/'))
);


// The set of Shibboleth attributes values that define a valid administrator
// only one item is needed, not all of them at once
$auth['shibboleth']['admin_roles'] = array(
  'unscoped-affiliation' => array('faculty','staff','employee'),
  'RolesTypeB2' => array('13000:75')
);
```

In this example all users which have affiliation _staff_,_faculty_ or _employee' 
will gain administrative access. The same for the users whose RolesTypeB2 attribute
contains the code _13000:75_ (which is equivalent to _employee_ affiliation in our case).

The users which have the affiliation _student_ or have their attribute RolesTypeB2 set
to _13000:30_ or _0:362_ get the role user. The last example is a regular expression
which is evaluated against all the roles the user has. This specific example matches
roles like _13123:190_ or _13321:190_ etc.

The administrator rules are stronger, i.e. once the user owns at least one 
sufficient role to gain administrator access she gains it. So be careful to 
define strong enough rules.
